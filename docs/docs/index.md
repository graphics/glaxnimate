<img src="/img/logo.svg" width="128" />

Glaxnimate is a simple and fast vector graphics animation program.

Here you can find technical documentation:


* [File Format](json_reference.md)
* [Scripting Reference](python_reference.md)

