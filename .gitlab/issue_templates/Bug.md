#### Steps to reproduce

<!-- Describe what you did (step-by-step) so we can reproduce: -->

* Open Glaxnimate
* ...

#### Result

<!-- Describe what happened and what the expected result would be -->

#### Version

<!-- List Operating System and Glaxnimate version
From Glaxnimate you can click on "Help > Copy Debug Information"
then paste here the information -->

<!-- Example file:
Attach a sample file (or files) highlighting the issue, if appropriate. -->
